package org.flightbooking.Repository;

import org.flightbooking.models.Flight;

import java.util.ArrayList;

public class FlightRepository {
    ArrayList flightLst = new ArrayList();

    public ArrayList addFlights()
    {
        Flight flt1 = new Flight(1,"AirBus",30,20,50,"Hyderabad","Bengaluru",100);
        Flight flt2 = new Flight(2,"Boeing",30,20,50,"Hyderabad","Delhi",100);
        Flight flt3 = new Flight(3,"SpiceJet",20,20,60,"Hyderabad","Mumbai",100);
        Flight flt4 = new Flight(4,"JetAirways",20,20,60,"Hyderabad","Mumbai",100);
        Flight flt5 = new Flight(5,"Indigo",20,20,60,"Hyderabad","Mumbai",100);
        Flight flt6 = new Flight(6,"KingFisher",20,20,60,"Hyderabad","Bengaluru",100);
        Flight flt7 = new Flight(7,"AirBus",20,20,60,"Bengaluru","Hyderabad",100);
        Flight flt8 = new Flight(8,"Boeing",20,20,60,"Mumbai","Delhi",100);
        Flight flt9 = new Flight(9,"SpiceJet",20,20,60,"Bengaluru","Delhi",100);
        Flight flt10 = new Flight(10,"JetAirways",20,20,60,"Delhi","Mumbai",100);
        Flight flt11 = new Flight(11,"AirBus",20,20,60,"Hyderabad","Mumbai",100);
        Flight flt12 = new Flight(12,"Boeing",20,20,60,"Hyderabad","Bengaluru",100);
        Flight flt13 = new Flight(13,"SpiceJet",20,20,60,"Mumbai","Delhi",100);
        Flight flt14 = new Flight(14,"JetAirways",20,20,60,"Bengaluru","Mumbai",100);
        Flight flt15 = new Flight(15,"Indigo",20,20,60,"Delhi","Mumbai",100);

        flightLst.add(flt1);
        flightLst.add(flt2);
        flightLst.add(flt3);
        flightLst.add(flt4);
        flightLst.add(flt5);
        flightLst.add(flt6);
        flightLst.add(flt7);
        flightLst.add(flt8);
        flightLst.add(flt9);
        flightLst.add(flt10);
        flightLst.add(flt11);
        flightLst.add(flt12);
        flightLst.add(flt13);
        flightLst.add(flt14);
        flightLst.add(flt15);

        return flightLst;

    }
}
