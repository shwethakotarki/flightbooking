package org.flightbooking.Services;

import org.flightbooking.Repository.FlightRepository;
import org.flightbooking.models.Flight;

import java.util.ArrayList;

public class FlightSearchService {

    private ArrayList flightLstfromRep = new ArrayList();
    ArrayList flightList = new ArrayList();


    public ArrayList AddFlights()
    {
        FlightRepository flRep = new FlightRepository();
        flightLstfromRep = flRep.addFlights();
        return flightLstfromRep;
    }

    public ArrayList searchOperatingFlights(String fromLocation, String toLocation, Integer noOfPass) {

        AddFlights();
        for (int i = 0; i < flightLstfromRep.size(); i++) {
            Flight t = (Flight) flightLstfromRep.get(i);
            if (t.getFromLocation().toUpperCase().equals(fromLocation.toUpperCase())&&
                    (t.getToLocation().toUpperCase().equals(toLocation.toUpperCase())) && noOfPass <= t.getTotalCapacity())
            {
                flightList.add(t);
            }
        }
        return flightList;

    }
}

