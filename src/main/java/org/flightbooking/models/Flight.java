package org.flightbooking.models;

public class Flight {
    Integer flightID;
    String flightModel;
    Integer FCcapacity;
    Integer BCcapacity;
    Integer Ecapacity;
    String fromLocation;
    String toLocation;
    Integer totalCapacity;

    public Integer getFlightID() {
        return flightID;
    }

    public void setFlightID(Integer flightID) {
        this.flightID = flightID;
    }

    public Flight()
    {

    }
    public Flight(Integer flightID,String flightModel, Integer FCcapacity, Integer BCcapacity, Integer ecapacity, String fromLocation, String toLocation,Integer totalCapacity) {
        this.flightID = flightID;
        this.flightModel = flightModel;
        this.FCcapacity = FCcapacity;
        this.BCcapacity = BCcapacity;
        Ecapacity = ecapacity;
        this.fromLocation = fromLocation;
        this.toLocation = toLocation;
        this.totalCapacity = totalCapacity;
    }

    public Integer getTotalCapacity() {
        return totalCapacity;
    }

    public void setTotalCapacity(Integer totalCapacity) {
        this.totalCapacity = totalCapacity;
    }

    public String getFromLocation() {
        return fromLocation;
    }

    public void setFromLocation(String fromLocation) {
        this.fromLocation = fromLocation;
    }

    public String getToLocation() {
        return toLocation;
    }

    public void setToLocation(String toLocation) {
        this.toLocation = toLocation;
    }


    public String getFlightModel() {
        return flightModel;
    }

    public void setFlightModel(String flightModel) {
        this.flightModel = flightModel;
    }

    public Integer getFCcapacity() {
        return FCcapacity;
    }

    public void setFCcapacity(Integer FCcapacity) {
        this.FCcapacity = FCcapacity;
    }

    public Integer getBCcapacity() {
        return BCcapacity;
    }

    public void setBCcapacity(Integer BCcapacity) {
        this.BCcapacity = BCcapacity;
    }

    public Integer getEcapacity() {
        return Ecapacity;
    }

    public void setEcapacity(Integer ecapacity) {
        Ecapacity = ecapacity;
    }
}
