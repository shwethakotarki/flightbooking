package org.flightbooking.controller;


import org.flightbooking.Repository.FlightRepository;
import org.flightbooking.Services.FlightSearchService;
import org.flightbooking.models.Flight;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Optional;

@org.springframework.web.bind.annotation.RestController
public class Controller {


    public String sayHi(@PathVariable String id){
        return "hello";}

   // ArrayList flightLstfromRep = new ArrayList();
    ArrayList flightList = new ArrayList();


    //@RequestMapping("/FlightBooking")
//    public ArrayList AddFlights()
//    {
//        FlightRepository flRep = new FlightRepository();
//        flightLstfromRep = flRep.addFlights();
//        return flightLstfromRep;
//    }



//    @RequestMapping("/Flight/{flightID}")
//    public String GetFlight(@PathVariable Integer flightID) {
//        String strFlight = "";
//        AddFlights();
//        for (int i = 0; i < flightLstfromRep.size(); i++) {
//            Flight t = (Flight) flightLstfromRep.get(i);
//            if (t.getFlightID() == flightID) {
//                strFlight =  (t.getFlightModel() + "\t " +t.getFromLocation() +"\t " +t.getToLocation() + " \t" + t.getEcapacity());
//            }
//        }
//        return strFlight;
//    }

//    @RequestMapping("/FlightSearch/{fromLocation}/{toLocation}/{noOfPass}")
//    public String searchOperatingFlights(@PathVariable String fromLocation, @PathVariable String toLocation, @PathVariable(noOfPass = "noOfPass", required = false) Integer noOfPass) {
//
//        Integer passenger = 1;
//        if(noOfPass.isPresent())
//        {
//            passenger = noOfPass.get();
//        }
//        FlightSearchService search = new FlightSearchService();
//        flightList = search.searchOperatingFlights(fromLocation, toLocation, passenger);
//        return displayFlights(flightList);
//    }
    @RequestMapping("/FlightSearch/{fromLocation}/{toLocation}/{noOfPass}")
   public String searchOperatingFlights(@PathVariable String fromLocation, @PathVariable String toLocation, @PathVariable Integer noOfPass) {

        FlightSearchService search = new FlightSearchService();
        flightList = search.searchOperatingFlights(fromLocation, toLocation, noOfPass);
        return displayFlights(flightList);
    }
    @RequestMapping("/FlightSearch/{fromLocation}/{toLocation}")
    public String searchOperatingFlights(@PathVariable String fromLocation, @PathVariable String toLocation) {

        Integer passenger = 1;

        FlightSearchService search = new FlightSearchService();
        flightList = search.searchOperatingFlights(fromLocation, toLocation, passenger);
        return displayFlights(flightList);
    }
    @RequestMapping("/Hello")
    public String Greeting() {
        return "Hello there on Monday";
    }

    public String displayFlights(ArrayList flightList)
    {
        String strflightList = "";

        for (int i = 0; i < flightList.size(); i++) {
            Flight t = (Flight) flightList.get(i);
            strflightList += t.getFlightModel() + "\t" +
                             t.getFromLocation() + "\t" + t.getToLocation() + "\t" + t.getTotalCapacity() + "<br />";
        }
        return strflightList;
    }


}
